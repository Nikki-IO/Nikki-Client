import * as React from "react"
import { useNavigate } from "react-router-dom"
import FormContainer from "../components/Auth/FormContainer"
import useAuth from "../hooks/useAuth"

function Landing() {

    const navigate = useNavigate()

    const auth = useAuth()

    React.useEffect(() => {
        if (auth.authenticated) navigate('/app', { replace: true })
    })

    return (
        <React.Fragment>
            <FormContainer />
        </React.Fragment>
    )
}

export default Landing