import * as React from "react"
import { Link } from "react-router-dom"
import useAuth from "../hooks/useAuth"
import ServersBar from "../components/ServersBar"
import ServerCreateForm from '../components/ServerCreateForm'

function Profile() {
    const [serverCreateFormVisible, showServerCreateForm] = React.useState(null)

    const { signout } = useAuth()

    const createServerPrompt = (event) => {
        event.preventDefault()
        showServerCreateForm(true)
    }

    const hideCreateServerForm = () => {
        showServerCreateForm(null)
    }

    return (
        <React.Fragment>
            {serverCreateFormVisible ?
                <ServerCreateForm hideCreateServerForm={hideCreateServerForm} /> : null}
            <div className="flex flex-row h-full items-center">
                <div className="flex flex-col h-full mt-40 px-4">
                    <ServersBar createServerPrompt={createServerPrompt} />
                    <button className="px-2 py-1 rounded bg-blue-600 text-gray-50"
                        onClick={() => signout()}>
                        Sign Out
                    </button>
                </div>
                <div className="flex-grow text-center py-64 rounded-lg bg-gray-300">
                    <span className="text-7xl">Welcome to Nikki!</span>
                </div>
            </div>
        </React.Fragment>
    )
}

export default Profile