import * as React from 'react'

function ServersBar(props) {
    return (
        <div className="flex flex-col my-4">
            <button className="bg-blue-800 text-gray-100 px-2 py-1 rounded"
                onClick={(event) => props.createServerPrompt(event)}>
                Create Server
            </button>
        </div>
    )
}

export default ServersBar