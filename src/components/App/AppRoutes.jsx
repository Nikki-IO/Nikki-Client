import { Routes, Route } from 'react-router-dom'
import Profile from '../../pages/Profile'

function AppRoutes() {
    return (
        <Routes>
            <Route path="/" element={<Profile />}></Route>
        </Routes>
    )
}

export default AppRoutes