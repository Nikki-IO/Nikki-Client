import * as React from 'react'
import AppRoutes from './AppRoutes'

function AuthorizedApp() {
    return (
            <AppRoutes />
    )
}

export default AuthorizedApp