import { React, useState } from "react"
import useAuth from "../../hooks/useAuth";

function SignUpForm() {
    const [inputs, setInputs] = useState({})

    const { signin } = useAuth()

    const handleChange = (event) => {
        const name = event.target.name
        const value = event.target.value
        setInputs(values => ({ ...values, [name]: value }))
    }

    const handleSubmit = async (event) => {
        event.preventDefault()

        const payload = inputs

        const response = await fetch('http://localhost:3001/auth/sign-up', {
            method: 'POST',
            mode: 'cors',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(payload)
        })

        const { token, user, err } = await response.json()

        if (err) {
            console.error(err)
        } else {
            localStorage.setItem('token', token)
            signin(user)
        }
    }

    return (
        <form className="flex flex-col px-2 py-2 gap-4"
            onSubmit={(event) => handleSubmit(event)}>
            <input
                type="text"
                name="user"
                value={inputs.user || ''}
                placeholder="Username"
                className="px-2 py-1"
                onChange={(event) => handleChange(event)}
            />
            <input
                type="email"
                name="email"
                value={inputs.email || ''}
                placeholder="Email"
                className="px-2 py-1"
                onChange={(event) => handleChange(event)}
            />
            <input
                type="password"
                name="password"
                value={inputs.password || ""}
                placeholder="Password"
                className="px-2 py-1"
                onChange={(event) => handleChange(event)}
            />
            <input
                type="submit"
                className="px-2 py-2 rounded bg-indigo-300 cursor-pointer" />
        </form>
    )
}

export default SignUpForm