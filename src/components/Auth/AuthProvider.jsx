import * as React from "react"
import { AuthContext } from "../../contexts/AuthContext"

const getUser = () => localStorage.getItem('user')

function AuthProvider({ children }) {

    const userState = getUser()

    let [user, setUser] = React.useState(userState)

    React.useEffect(() => {
    }, [])

    const signin = (user) => {
        setUser(user)
        localStorage.setItem('user', user)
    }

    const signout = () => {
        setUser(null)
        localStorage.removeItem('user')
        localStorage.removeItem('token')
    }

    const value = { user, signin, signout }

    return <AuthContext.Provider value={value}>{children}</AuthContext.Provider>
}

export default AuthProvider