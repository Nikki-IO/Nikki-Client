import * as React from "react"

function ServerCreateForm(props) {
    return (
        <div className="modal fixed w-full h-full top-0 left-0 flex items-center justify-center">
            <div
                className="modal-overlay absolute w-full h-full bg-gray-900 opacity-50"
                onClick={() => props.hideCreateServerForm()}
            />
            <div className="modal-container bg-white w-11/12 md:max-w-md mx-auto rounded shadow-lg z-50 overflow-y-auto">

                {/*<!-- Add margin if you want to see some of the overlay behind the modal-->*/}
                <div className="modal-content py-4 text-left px-6">
                    {/*<!--Title-->*/}
                    <div className="flex justify-between items-center pb-3">
                        <span className="text-2xl font-bold">Create Server</span>
                    </div>
                    {/*<!--Body-->*/}
                    <form>
                        <input
                            className="px-2 py-2 outline-none border-2 rounded-md border-gray-800 border-opacity-50"
                            type="text"
                            placeholder="Name"
                        />
                    </form>

                    {/*<!--Footer-->*/}
                    <div className="flex justify-end mt-2">
                        <button className="px-4 bg-transparent p-3 rounded-lg text-indigo-500 hover:bg-gray-100 hover:text-indigo-400 mr-2">
                            Action
                        </button>
                        <button
                            className="modal-close px-4 bg-indigo-500 p-3 rounded-lg text-white hover:bg-indigo-400"
                            onClick={() => props.hideCreateServerForm()}>
                            Close
                        </button>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default ServerCreateForm